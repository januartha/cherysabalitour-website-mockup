import menu from '@/assets/data/menu'
import destinations from '@/assets/data/destinations'
import tours from '@/assets/data/tours'
import hotels from '@/assets/data/hotels'
import carRental from '@/assets/data/car-rental'

describe('menu', () => {
    test('should have menu property and is Array', () => {
        expect(menu.hasOwnProperty('menu')).toBe(true)
        expect(menu.menu.constructor === Array).toBe(true)
    })
    test('menu items should have properties: name, path and correct data type', () => {
        expect(menu.menu.filter(item => {
            return  item.name && item.path &&
                    item.name.constructor === String &&
                    item.path.constructor === String
        })).toHaveLength(menu.menu.length)
    })
    test('each submenu item should have properties: name, path and correct data type', () => {
        menu.menu.filter(item => item.submenu).map(item => {
            expect(item.submenu.filter(item => {
                return  item.name && item.path &&
                        item.name.constructor === String &&
                        item.path.constructor === String
            })).toHaveLength(item.submenu.length)
        })
    })
})

describe('destinations', () => {
    test('should have packages property and is Array', () => {
        expect(destinations.hasOwnProperty('destinationsList')).toBe(true)
        expect(destinations.destinationsList.constructor === Array).toBe(true)
    })
    test('each deestinationsList item should have properties: name, img, desc, btnLabel, btnUrl, detailUrl, details', () => {
        expect(destinations.destinationsList.filter(item => {
            return  item.name && item.img && item.desc && 
                    item.btnLabel && item.btnUrl &&
                    item.detailUrl && item.details
        })).toHaveLength(destinations.destinationsList.length)
    })
    test('each destinationsList item should have correct properties data type', () => {
        expect(destinations.destinationsList.filter(item => {
            return  item.name.constructor === String &&
                    item.img.constructor === String &&
                    item.desc.constructor === String &&
                    item.btnLabel.constructor === String &&
                    item.btnUrl.constructor === String &&
                    item.detailUrl.constructor === String &&
                    item.details.constructor === Array
        })).toHaveLength(destinations.destinationsList.length)
    })
})

describe('tours', () => {
    test('should have packages property and is Array', () => {
        expect(tours.hasOwnProperty('packages')).toBe(true)
        expect(tours.packages.constructor === Array).toBe(true)
    })
    test('each packages item should have properties: name, img, desc, pageDesc, detailUrl, details', () => {
        expect(tours.packages.filter(item => {
            return  item.name && item.img && item.desc && item.pageDesc && 
                    item.detailUrl && item.details
        })).toHaveLength(tours.packages.length)
    })
    test('each packages item should have correct properties data type', () => {
        expect(tours.packages.filter(item => {
            return  item.name.constructor === String &&
                    item.img.constructor === String &&
                    item.desc.constructor === String &&
                    item.pageDesc.constructor === String &&
                    item.detailUrl.constructor === String &&
                    item.details.constructor === Array
        })).toHaveLength(tours.packages.length)
    })
})

describe('hotels', () => {
    test('should have hotelsList property and is Array', () => {
        expect(hotels.hasOwnProperty('hotelsList')).toBe(true)
        expect(hotels.hotelsList.constructor === Array).toBe(true)
    })
    test('each hotelsList item should have properties: name, img, desc, btnLabel, btnUrl', () => {
        expect(hotels.hotelsList.filter(item => {
            return  item.name && item.img && item.desc && 
                    item.btnLabel && item.btnUrl
        })).toHaveLength(hotels.hotelsList.length)
    })
    test('each hotelsList item should have correct properties data type', () => {
        expect(hotels.hotelsList.filter(item => {
            return  item.name.constructor === String &&
                    item.img.constructor === String &&
                    item.desc.constructor === String &&
                    item.btnLabel.constructor === String &&
                    item.btnUrl.constructor === String
        })).toHaveLength(hotels.hotelsList.length)
    })
})

describe('carRental', () => {
    test('should have carsList property and is Array', () => {
        expect(carRental.hasOwnProperty('carsList')).toBe(true)
        expect(carRental.carsList.constructor === Array).toBe(true)
    })
    test('each carsList item should have properties: name, img, desc, btnLabel, btnUrl', () => {
        expect(carRental.carsList.filter(item => {
            return  item.name && item.img && item.desc && 
                    item.btnLabel && item.btnUrl
        })).toHaveLength(carRental.carsList.length)
    })
    test('each carsList item should have correct properties data type', () => {
        expect(carRental.carsList.filter(item => {
            return  item.name.constructor === String &&
                    item.img.constructor === String &&
                    item.desc.constructor === String &&
                    item.btnLabel.constructor === String &&
                    item.btnUrl.constructor === String
        })).toHaveLength(carRental.carsList.length)
    })
})