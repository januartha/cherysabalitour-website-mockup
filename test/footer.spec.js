import { shallowMount } from '@vue/test-utils'
import NuxtLink from '@/.nuxt/components/nuxt-link'
import Footer from '@/components/footer'

describe("footer.vue", () => {
    let wrapper
    let $store = {
        state: {
            pageError: false
        }
    }
    let $route = {
        name: 'index'
    }
    beforeEach(() => {
        Footer.components = Footer.components || {}
        Footer.components.NuxtLink = NuxtLink
        wrapper = shallowMount(Footer, {
            attachToDocument: true,
            mocks: {
                $route,
                $store
            }  
        })
    })
    test('render footer', () => {
        expect(wrapper.find('#footer').exists()).toBe(true)
    })
})