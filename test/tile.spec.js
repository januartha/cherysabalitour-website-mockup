import { shallowMount } from '@vue/test-utils'
import NuxtLink from '@/.nuxt/components/nuxt-link'
import Tile from '@/components/tile'
import data from '@/assets/data/destinations'

describe("tile.vue", () => {
    let wrapper
    let items = data.destinationsList
    beforeEach(() => {
        Tile.components = Tile.components || {}
        Tile.components.NuxtLink = NuxtLink
        wrapper = shallowMount(Tile, {
            attachToDocument: true,
            propsData: {
                list: items
            }
        })
    })
    test('list is Array', () => {
        wrapper.setProps({
            list: [1, 2]
        })
        expect(wrapper.vm.list.constructor === Array).toBe(true)
    })
    test('list item should contains: name, img, desc as String type', () => {
        expect(wrapper.vm.list.filter(item => {
            return  item.name && item.img && item.desc &&
                    item.name.constructor === String &&
                    item.img.constructor === String &&
                    item.desc.constructor === String
        })).toHaveLength(items.length)
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('default mode HAS tile-item--info', () => {
        expect(wrapper.contains('.tile-item--info')).toBe(true)
    })
    test('thumbnail mode has NO tile-item--info', () => {
        wrapper.setProps({
            thumbnailMode: true
        })
        expect(wrapper.contains('.tile-item--info')).toBe(false)
    })
    test('default mode image placeHolder should /images/assets/3x2-placeholder.svg', () => {
        expect(wrapper.vm.placeHolder).toBe('/images/assets/3x2-placeholder.svg')
    })
    test('square mode image placeHolder should /images/assets/2x2-placeholder.svg', () => {
        wrapper.setProps({
            square: true
        })
        expect(wrapper.vm.placeHolder).toBe('/images/assets/2x2-placeholder.svg')
    })
    test('render all items', () => {
        let items = [1, 2, 3, 4, 5, 6, 7, 8]
        wrapper.setProps({
            list: items
        })
        expect(wrapper.findAll('li')).toHaveLength(items.length)
    })
    test('num-columns should max 3 columns', () => {
        let numColumns = 6
        let items = [1, 2, 3, 4, 5, 6, 7, 8]
        wrapper.setProps({
            list: items,
            numColumns
        })
        expect(wrapper.find(".tile-item--col" + (numColumns > 3 ? 3 : numColumns)).exists()).toBe(true)
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('6 tiles with 3 columns, should contain last-2nd class', () => {
        let numColumns = 3
        let items = [1, 2, 3, 4, 5, 6]
        wrapper.setProps({
            list: items,
            numColumns
        })
        expect(wrapper.find(".last-2nd").exists()).toBe(true)
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('5 tiles with 2 columns, last item should centered with "last-center" class', () => {
        let numColumns = 2
        let items = [1, 2, 3, 4, 5]
        wrapper.setProps({
            list: items,
            numColumns
        })
        expect(wrapper.find(".last-center").exists()).toBe(true)
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('should a target to "_blank" when url started with "http"', () => {
        expect(wrapper.vm.generateURLTarget("http://yourdomain.com/page")).toEqual('_blank')
        expect(wrapper.vm.generateURLTarget("/path/page")).not.toEqual('_blank')
        expect(wrapper.vm.generateURLTarget()).not.toEqual('_blank')
    })
})