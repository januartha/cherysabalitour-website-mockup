import { shallowMount } from '@vue/test-utils'
import NuxtLink from '@/.nuxt/components/nuxt-link'
import ChessGrid from '@/components/chess-grid'
import data from '@/assets/data/index'

describe("chess-grid.vue", () => {
    let wrapper
    let items = [1, 2, 3, 4, 5, 6, 7, 8]
    beforeEach(() => {
        ChessGrid.components = ChessGrid.components || {}
        ChessGrid.components.NuxtLink = NuxtLink
        wrapper = shallowMount(ChessGrid, {
            propsData: {
                list: items
            }
        })
    })
    test('list is Array', () => {
        expect(wrapper.vm.list.constructor === Array).toBe(true)
    })
    test('list item should contains: name, img, desc', () => {
        let items = data.destinationList
        wrapper.setProps({
            list: items
        })
        expect(wrapper.vm.list.filter(item => (item.name && item.img && item.desc))).toHaveLength(items.length)
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('render all items', () => {
        expect(wrapper.findAll('.chess-grid-item')).toHaveLength(items.length)
    })
    test('pull and push odd items', () => {
        let oddItem = wrapper.findAll('.chess-grid-item').at(4)
        expect(oddItem.findAll('.chess-grid-item--box').at(0).is('.pull')).toBe(true)
        expect(oddItem.findAll('.chess-grid-item--box').at(1).is('.push')).toBe(true)
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('should pull and push element index === odd number', () => {
        expect(wrapper.vm.isPull(0)).toEqual(' pull')
        expect(wrapper.vm.isPush(0)).toEqual(' push')
    })
    test('should a target to "_blank" when url started with "http/https"', () => {
        expect(wrapper.vm.generateURLTarget("http://yourdomain.com/page")).toEqual('_blank')
        expect(wrapper.vm.generateURLTarget("/path/page")).toEqual('_self')
    })
})