import { shallowMount } from '@vue/test-utils'
import Page from '@/pages/index'
import { wrap } from 'module';

describe("page.js", () => {
    let wrapper
    let $store = {
        state: {
            pageData: {
                name: "Page Name",
                pageDesc: "Page description"
            },
            pageDesc: "Page description",
            loading: false,
            initiated: false
        }
    }
    let $route = {
        name: 'index'
    }
    beforeEach(() => {
        wrapper = shallowMount(Page, {
            mocks: {
                $route,
                $store
            }
        })
    })
    test('should remove HTML tags', () => {
        expect(wrapper.vm.removeHTMLTags('<b>hello</b>')).toEqual('hello')
        expect(wrapper.vm.removeHTMLTags('<span class="title">hello</span> <i>world</i>')).toEqual('hello world')
    })
})