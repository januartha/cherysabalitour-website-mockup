import { mount, RouterLinkStub } from '@vue/test-utils'
import NuxtLink from '@/.nuxt/components/nuxt-link'
import ListBox from '@/components/list-box'
import destinations from '@/assets/data/destinations'
import tours from '@/assets/data/tours'

describe("list-box.vue", () => {
    let wrapper
    let items = destinations.destinationsList[0].details
    beforeEach(() => {
        ListBox.components = ListBox.components || {}
        ListBox.components.NuxtLink = NuxtLink
        wrapper = mount(ListBox, {
            propsData: {
                list: items
            },
            stubs: {
                RouterLink: RouterLinkStub
            }
        })
    })
    test('list is Array', () => {
        wrapper.setProps({
            list: [1, 2, 3, 4]
        })
        expect(wrapper.vm.list.constructor === Array).toBe(true)
    })
    test('valid item properties: list, customList, customListWithNumber, slides, googleMap, googleReview, table', () => {
        //test destinations pages
        wrapper.setProps({
            list: destinations.destinationsList[0].details
        })
        expect(wrapper.vm.list.filter(item => {
            return  item.hasOwnProperty("list") ||
                    item.hasOwnProperty("customList") ||
                    item.hasOwnProperty("customListWithNumber") ||
                    item.hasOwnProperty("slides") ||
                    item.hasOwnProperty("googleMap") ||
                    item.hasOwnProperty("googleReview") ||
                    item.hasOwnProperty("table") ||
                    item.hasOwnProperty("note")
        })).toHaveLength(wrapper.vm.list.length)
        //test tours pages
        wrapper.setProps({
            list: tours.packages[0].details
        })
        expect(wrapper.vm.list.filter(item => {
            return  item.hasOwnProperty("list") ||
                    item.hasOwnProperty("customList") ||
                    item.hasOwnProperty("customListWithNumber") ||
                    item.hasOwnProperty("slides") ||
                    item.hasOwnProperty("googleMap") ||
                    item.hasOwnProperty("googleReview") ||
                    item.hasOwnProperty("table") ||
                    item.hasOwnProperty("note")
        })).toHaveLength(wrapper.vm.list.length)
    })
    test('render google-map if contain googleMap', () => {
        if(wrapper.vm.list.find(item => item.googleMap))
        {
            expect(wrapper.find('#map').exists()).toBe(true)
        }
        //expect(wrapper.html()).toMatchSnapshot()
    })
    test('render google-review if contain googleReview', () => {
        if(wrapper.vm.list.find(item => item.googleReview))
        {
            expect(wrapper.find('#google-review').exists()).toBe(true)
        }
    })
    test('render slides if contain slides', () => {
        if(wrapper.vm.list.find(item => item.slides))
        {
            expect(wrapper.find('.list-box-slide').exists()).toBe(true)
        }
    })
    test('render table if contain table', () => {
        wrapper.setProps({
            list: tours.packages[0].details
        })
        if(wrapper.vm.list.find(item => item.table))
        {
            expect(wrapper.find('table').exists()).toBe(true)
        }
        //expect(wrapper.html()).toMatchSnapshot()
    })
})