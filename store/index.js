import Vuex from "vuex"

const createStore = () => {
    const state = {
        placeID: "ChIJU2YfX3tx0i0RJHKN6caSY4k",
        pageData: null,
        pageQuote: null,
        pageDesc: null,
        pageError: false,
        loading: false,
        initiated: false
    }

    const getters = {}

    const mutations = {
        //setPageDesc({store}, payload)
    }

    const actions = {
        
    }

    return new Vuex.Store ({
        state, getters, mutations, actions
    })
}

export default createStore