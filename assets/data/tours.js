module.exports = {
    packages:[
        {
            name: "One Day Tour Nusa Penida",
            img: "images/contents/crystal-bay-beach-mid-sq.jpg",
            desc: "One day tour within Nusa Penida:<br>Tree of Love and Kelingking Beach<br>Angel Billabong and Broken Beach<br>Crystal Bay Beach",
            pageDesc: "<p>We manage a full day tour to fit your time and budget but still enough to enjoy and get experience on exploring magical Nusa Penida tourist destinations.</p>",
            detailUrl: "/tours/one-day-tour-nusa-penida",
            details:[
                {
                    name: "Itinerary",
                    list:[
                        "07:30 Meeting point at Sanur Beach",
                        "08:00 Go to Nusa Penida with fast boat",
                        "09:30 Visit Tree of Love and Kelingking Beach", 
                        "11:30 Visit Angel Billabong and Broken Beach",
                        "12:30 Lunch at Angel Billabong Restaurant",
                        "14:00 Visit Crystal Bay Beach",
                        "16:00 Back to Harbor"
                    ]
                },
                {
                    name: "Tour Destinations",
                    slides: [
                        {name: "Tree of Love", img: "images/contents/tree-of-love.jpg"},
                        {name: "Kelingking Beach", img: "images/contents/klingking-beach.jpg"},
                        {name: "Angel Billabong", img: "images/contents/angel-billabong.jpg"},
                        {name: "Broken Beach", img: "images/contents/broken-beach.jpg"},
                        {name: "Crystal Bay Beach", img: "images/contents/crystal-bay-beach.jpg"}
                    ]
                },
                {
                    name: "Price List",
                    table: [
                        {p: 1, d:"Rp 850.000", i:"Rp 1.050.000"},
                        {p: 2, d:"Rp 575.000", i:"Rp 700.000"},
                        {p: 3, d:"Rp 475.000", i:"Rp 650.000"},
                        {p: 4, d:"Rp 450.000", i:"Rp 600.000"},
                        {p: 5, d:"Rp 425.000", i:"Rp 575.000"},
                        {p: 6, d:"Rp 400.000", i:"Rp 550.000"}
                    ],
                    btnLabel: "Book Now",
                    btnUrl: "/book-now?type=booking-tour",
                    btnBookNow: true
                },
                {
                    name: "Package Inclusion",
                    list:[
                        "Fast Boat Return Ticket",
                        "Transport at Nusa Penida",
                        "Lunch",
                        "Mineral Water"
                    ]
                }
            ],
        },
        {
            name: "One Day Tour Nusa Penida plus Snorkeling",
            img: "images/contents/snorkeling-crystal-bay-beach-mid-sq.jpg",
            desc: "One day tour within Nusa Penida:<br>Tree of Love and Kelingking Beach<br>Angel Billabong and Broken Beach<br>Crystal Bay Beach<br>Snorkeling",
            pageDesc: "<p>We manage a full day tour plus snorkeling for anyone who has limited time and budget to enjoy magical Nusa Penida tourist destinations including underwater tour by snorkeling and if you're lucky you may met Mola-mola fish or event Manta Ray.</p>",
            detailUrl: "/tours/one-day-tour-nusa-penida-plus-snorkeling",
            details:[
                {
                    name: "Itinerary",
                    list:[
                        "07:30 Meeting point at Sanur Beach",
                        "08:00 Go to Nusa Penida with fast boat",
                        "09:30 Visit Tree of Love and Klingking Beach", 
                        "11:30 Visit Angel Billabong and Broken Beach",
                        "12:30 Lunch at Angel Billabong Restaurant",
                        "14:00 Visit Crystal Bay Beach",
                        "16:00 Back to Harbor"
                    ]
                },
                {
                    name: "Tour Destinations",
                    slides: [
                        {name: "Tree of Love", img: "images/contents/tree-of-love.jpg"},
                        {name: "Kelingking Beach", img: "images/contents/klingking-beach.jpg"},
                        {name: "Angel Billabong", img: "images/contents/angel-billabong.jpg"},
                        {name: "Broken Beach", img: "images/contents/broken-beach.jpg"},
                        {name: "Crystal Bay Beach", img: "images/contents/crystal-bay-beach.jpg"},
                        {name: "Snorkeling at Nusa Penida", img: "images/contents/snorkeling-crystal-bay-beach.jpg"}
                    ]
                },
                {
                    name: "Price List",
                    table: [
                        {p: 1, d:"Rp 1050.000", i:"Rp 1.250.000"},
                        {p: 2, d:"Rp 775.000", i:"Rp 900.000"},
                        {p: 3, d:"Rp 675.000", i:"Rp 850.000"},
                        {p: 4, d:"Rp 650.000", i:"Rp 800.000"},
                        {p: 5, d:"Rp 625.000", i:"Rp 775.000"},
                        {p: 6, d:"Rp 600.000", i:"Rp 750.000"}
                    ],
                    btnLabel: "Book Now",
                    btnUrl: "/book-now?type=booking-tour",
                    btnBookNow: true
                },
                {
                    name: "Package Inclusion",
                    list:[
                        "Fast Boat Return Ticket",
                        "Transport at Nusa Penida",
                        "Lunch",
                        "Mineral Water",
                        "Life Jacket",
                        "Snokel + Pin",
                        "Underwater Photos"
                    ]
                }
            ]
        },
        {
            name: "2 Days 1 Night Tour Nusa Penida",
            img: "images/contents/raja-lima-sunset-mid-sq.jpg",
            desc: "2 days plus 1 night exploring Nusa Penida:<br>Tree of Love and Kelingking Beach<br>Angel Billabong and Broken Beach<br>Crystal bay Beach<br>Teletubbies Hill<br>Raja Lima and Tree House<br>Atuh Beach<br>Giri Putri Cave",
            pageDesc: "<p>Cherysa Tour has select tourist destionations in Nusa Penida from 14 villages to fit as 2 days and 1 night tour. We will guiding you to explore West Nusa Penida Tour and East Nusa Penida Tour.</p>",
            detailUrl: "/tours/2-days-1-night-tour-nusa-penida",
            details:[
                {
                    name: "Itinerary",
                    customListWithNumber:[
                        {
                            name:"Day 1",
                            items:[
                                "07:30 Meeting point at Sanur Beach",
                                "08:00 Go to Nusa Penida with fast boat",
                                "09:30 Visit Tree of Love and Klingking Beach", 
                                "11:30 Visit Angel Billabong and Broken Beach",
                                "12:30 Lunch at Angel Billabong Restaurant",
                                "15:00 Visit Crystal Bay Beach",
                                "18:00 Back to Hotel"
                            ]
                        },
                        {
                            name:"Day 2",
                            items:[
                                "09:00 Visit Teletubbies Hill",
                                "10:00 Visit Raja Lima and Tree House",
                                "12:00 Lunch",
                                "13:00 Visit Atuh Beach",
                                "15:00 Visit Giri Putri Cave",
                                "16:30 Back to Harbor"
                            ]
                        }
                    ]
                },
                {
                    name: "West Nusa Penida Tour Destinations - Day 1",
                    slides: [
                        {name: "Tree of Love", img: "images/contents/tree-of-love.jpg"},
                        {name: "Kelingking Beach", img: "images/contents/klingking-beach.jpg"},
                        {name: "Angel Billabong", img: "images/contents/angel-billabong.jpg"},
                        {name: "Broken Beach", img: "images/contents/broken-beach.jpg"},
                        {name: "Crystal Bay Beach", img: "images/contents/crystal-bay-beach-2.jpg"}
                    ]
                },
                {
                    name: "East Nusa Penida Tour Destinations - Day 2",
                    slides: [
                        {name: "Teletubbies Hills", img: "images/contents/teletubbies-hills.jpg"},
                        {name: "Raja Lima", img: "images/contents/raja-lima.jpg"},
                        {name: "Tree House", img: "images/contents/tree-house.jpg"},
                        {name: "Atuh Beach", img: "images/contents/atuh-beach.jpg"},
                        {name: "Giri Putri Cave", img: "images/contents/giri-putri-cave.jpg"}
                    ]
                },
                {
                    name: "Price List 1",
                    subname: "Bintang Bungalows or Nusa Penida Beach Hotel",
                    table: [
                        {p: 1, d:"Rp 2.250.000", i:"Rp 2.400.000"},
                        {p: 2, d:"Rp 1.400.000", i:"Rp 1.650.000"},
                        {p: 3, d:"Rp 1.100.000", i:"Rp 1.300.000"},
                        {p: 4, d:"Rp 900.000", i:"Rp 1.100.000"},
                        {p: 5, d:"Rp 825.000", i:"Rp 975.000"},
                        {p: 6, d:"Rp 750.000", i:"Rp 850.000"}
                    ],
                    btnLabel: "Book Now",
                    btnUrl: "/book-now?type=booking-tour",
                    btnBookNow: true
                },
                {
                    name: "Price List 2",
                    subname: "Caspla Bali or Semabu Hills Hotel",
                    table: [
                        {p: 1, d:"Rp 2.600.000", i:"Rp 2.700.000"},
                        {p: 2, d:"Rp 1.650.000", i:"Rp 1.950.000"},
                        {p: 3, d:"Rp 1.450.000", i:"Rp 1.750.000"},
                        {p: 4, d:"Rp 1.100.000", i:"Rp 1.400.000"},
                        {p: 5, d:"Rp 1.000.000", i:"Rp 1.300.000"},
                        {p: 6, d:"Rp 950.000", i:"Rp 1.150.000"}
                    ],
                    btnLabel: "Book Now",
                    btnUrl: "/book-now?type=booking-tour",
                    btnBookNow: true
                },
                {
                    name: "Package Inclusion",
                    list:[
                        "Fast Boat Return Ticket",
                        "Transport at Nusa Penida",
                        "Breakfast 1x",
                        "Lunch 2x",
                        "Mineral Water",
                        "Wellcome drink",
                        "Hotel Room"
                    ]
                }
            ]
        },
        {
            name: "One Day Tour Nusa Lembongan",
            img: "images/contents/devils-tears-mid-sq.jpg",
            desc: "One day tour within Nusa Lembongan:<br>Panorama Point<br>Dream Beach<br>Devil's Tears<br>Yellow Bridge<br>Mangrove Forest",
            pageDesc: "<p>We manage a full day tour to fit your time and budget but still enough to enjoy and get experience on exploring magical Nusa Lembongan tourist destinations.</p>",
            detailUrl: "/tours/one-day-tour-nusa-lembongan",
            details:[
                {
                    name: "Itinerary",
                    list:[
                        "08:30 Meeting point at Sanur Beach",
                        "09:00 Go to Nusa Lembongan with fast boat",
                        "09:30 Visit Panorama Point", 
                        "10:30 Visit Dream Beach",
                        "11:00 Visit Devils Tears",
                        "12:00 Visit Yellow Bridge",
                        "13:00 Lunch",
                        "14:30 Visit Mangrove Forest",
                        "15:30 Back to Harbor"
                    ]
                },
                {
                    name: "Tour Destinations",
                    slides: [
                        {name: "Panorama Point", img: "images/contents/panorama-point.jpg"},
                        {name: "Dream Beach", img: "images/contents/dream-beach.jpg"},
                        {name: "Devil's Tears", img: "images/contents/devils-tears.jpg"},
                        {name: "Yellow Bridge", img: "images/contents/yellow-bridge.jpg"},
                        {name: "Mangrove Forest", img: "images/contents/mangrove-forest.jpg"}
                    ]
                },
                {
                    name: "Price List",
                    table: [
                        {p: 1, d:"Rp 900.000", i:"Rp 1.100.000"},
                        {p: 2, d:"Rp 625.000", i:"Rp 750.000"},
                        {p: 3, d:"Rp 525.000", i:"Rp 700.000"},
                        {p: 4, d:"Rp 500.000", i:"Rp 650.000"},
                        {p: 5, d:"Rp 475.000", i:"Rp 625.000"},
                        {p: 6, d:"Rp 450.000", i:"Rp 600.000"}
                    ],
                    btnLabel: "Book Now",
                    btnUrl: "/book-now?type=booking-tour",
                    btnBookNow: true
                },
                {
                    name: "Package Inclusion",
                    list:[
                        "Fast Boat Return Ticket",
                        "Transport at Nusa Lembongan",
                        "Ticket / Entrance Fee",
                        "Lunch",
                        "Mineral Water"
                    ]
                }
            ]
        }
    ]
}