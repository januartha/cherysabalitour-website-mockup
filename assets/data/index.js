module.exports = {
    destinationList: [
        {
            id:0,
            name: "One Day Tour Nusa Penida",
            img: "images/contents/crystal-bay-beach-mid-sq.jpg",
            desc: "One day tour exploring Nusa Penida:<br>Tree of Love and Klingking Beach,<br>Broken Beach and Angel Billabong,<br>Crystal Bay Beach<br><br><b>Start from Rp 400.000</b>",
            btnLabel: "More Details",
            url: "/tours/one-day-tour-nusa-penida"
        },
        {
            id: 1,
            name: "Snorkeling at Nusa Penida Beach",
            img: "images/contents/snorkeling-crystal-bay-beach-mid-sq.jpg",
            desc: "Enjoy beautiful underwater scene in Nusa Penida Beach<br>If you're lucky you may met Manta Ray or Mola-mola<br><br><b>Start from Rp 600.000</b>",
            btnLabel: "More Details",
            url: "/tours/one-day-tour-nusa-penida-plus-snorkeling"
        },
        {
            id: 2,
            name: "2 Days and 1 Night Tour Nusa Penida",
            img: "images/contents/raja-lima-sunset-mid-sq.jpg",
            desc: "Complete 2 days and 1 night tour covering west and east side of Nusa Penida famous tourist destionations<br><br><b>Start from Rp 750.000</b>",
            btnLabel: "more details",
            url: "/tours/2-days-1-night-tour-nusa-penida"
        },
        {
            id: 3,
            name: "One Day Tour Nusa Lembongan",
            desc: "One day tour within Nusa Lembongan:<br>Panorama Point, Dream Beach, Devil's Tears, Yellow Bridge and Mangrove Forest<br><br><b>Start from Rp 450.000</b>",
            img: "images/contents/devils-tears-mid-sq.jpg",
            btnLabel: "more details",
            url: "/tours/one-day-tour-nusa-lembongan"
        },
        {
            id: 4,
            name: "Nusa Penida Magical Place",
            desc: "You will guided to visiting Giri Putri Cave, a magical place inside the cave where you will discover a place of worship Hindu devotees and also Buddist and Confucian followers<br><br><b>Start from Rp 750.000</b>",
            img: "images/contents/giri-putri-cave-mid-sq.jpg",
            btnLabel: "more details",
            url: "/tours/2-days-1-night-tour-nusa-penida"
        }
        
    ]
}