module.exports = {
    carsList:[
        {
            name: "Toyota Avanza",
            img: "images/contents/car-toyota-avanza.jpg",
            desc: "Price With Driver:<br><b>USD 90 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 5 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Suzuki APV",
            img: "images/contents/car-suzuki-apv.jpg",
            desc: "Price With Driver:<br><b>USD 85 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 5 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Suzuki Ertiga",
            img: "images/contents/car-suzuki-ertiga.jpg",
            desc: "Price With Driver:<br><b>USD 90 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 5 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Toyota Innova",
            img: "images/contents/car-toyota-innova.jpg",
            desc: "Price With Driver:<br><b>USD 130 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 5 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Kia Pregio",
            img: "images/contents/car-kia-pregio.jpg",
            desc: "Price With Driver:<br><b>USD 100 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 12 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Kia Travello",
            img: "images/contents/car-kia-travello.jpg",
            desc: "Price With Driver:<br><b>USD 100 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 12 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Isuzu Elf",
            img: "images/contents/car-isuzu-elf.jpg",
            desc: "Price With Driver:<br><b>USD 130 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 12 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Isuzu Elf Long",
            img: "images/contents/car-isuzu-elf-long.jpg",
            desc: "Price With Driver:<br><b>USD 130 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 12 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        },
        {
            name: "Toyota Hi Ace",
            img: "images/contents/car-toyota-hi-ace.jpg",
            desc: "Price With Driver:<br><b>USD 130 / 10 hours</b><br><br>Price Include:<br>Petrol<br>Air condition vehicles<br>Driver speak English<br>Capacity: passenger max 9 pax<br>Departure on Request",
            btnLabel: "Book Now",
            btnUrl: "book-now?type=booking-car"
        }
    ]
}