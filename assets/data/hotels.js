module.exports = {
    hotelsList:[
        {
            name: "Nusa Penida Beach Hotel",
            img: "images/contents/hotel-nusa-penida-beach-mid.jpg",
            desc: "Conveniently located in Bali, Nusa Penida Beach Hotel is a great base from which to explore this vibrant city. Situated only 20 km from the city center, guests are well located to enjoy the town's attractions and activities. With its convenient location, the hotel offers easy access to the city's must-see destinations.<br><br>Nusa Penida Beach Hotel also offers many facilities to enrich your stay in Bali. This hotel offers numerous on-site facilities to satisfy even the most discerning guest.",
            btnLabel: "Book Now",
            btnUrl: "/book-now?type=booking-tour"
        },
        {
            name: "Bintang Bungalows",
            img: "images/contents/hotel-bintang-bungalows-mid.jpg",
            desc: "Bintang Bungalow is located in Toyapakeh just about 800 meters from Crystal Bay. The hotel is a 15-minutes drive to Gamat Bay and 30-minutes to Angel's Billabong.<br><br>Features an outdoor pool. You can enjoy meals at the restaurant at the accommodation.",
            btnLabel: "Book Now",
            btnUrl: "/book-now?type=booking-tour"
        },
        {
            name: "Caspla Beach Hotel",
            img: "images/contents/hotel-caspla-beach-mid.jpg",
            desc: "Caspla Beach Hotel, Restaurant & Bar is built seashore side on the easthern most tip of Bali, that is Nusa Penida Island.<br><br>Enjoy luxury hotels on east Bali and an unforgettable holiday in Caspla Beach Hotel, Bar & Restaurant unique, elegant, tropical style, ocean front bungalows. the hotel building full of wood, here you can enjoy free pool, eating at the beachside restaurant.",
            btnLabel: "Book Now",
            btnUrl: "/book-now?type=booking-tour"
        },
        {
            name: "Semabu Hills Hotel",
            img: "images/contents/hotel-semabu-hills-mid.jpg",
            desc: "It can be argued that Bali is as close to heaven on earth as it gets and we at Semabu Hills have put in place the crowning glory of this picturesque getaway.<br><br>Just 30 minutes off the east coast of Bali on the tropical island of Nusa Penida, Semabu Hills is proud to host you and give you an experience to cherish.",
            btnLabel: "Book Now",
            btnUrl: "/book-now?type=booking-tour"
        }
    ]
}