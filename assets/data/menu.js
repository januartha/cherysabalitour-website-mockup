module.exports = {
    menu: [
        {
            name: "Home", path: "/", quote: "Cherysa Tour is a Bali and Nusa Penida Tour Agency"
        },
        {
            name: "Destinations", path: "/destinations", quote: "9 Awesome Tourist Destinations in Nusa Penida and Nusa Lembongan"
        },
        {
            name: "Tours", path: "/tours", quote: "Exploring Nusa Penida with Cherysa Tour", submenu: [
                { name: "One Day Tour Nusa Penida", path: "/tours/one-day-tour-nusa-penida", quote: "One Day Nusa Penida Tour Package" },
                { name: "One Day Tour Nusa Penida + Snorkeling", path: "/tours/one-day-tour-nusa-penida-plus-snorkeling", quote: "One Day Nusa Penida Tour plus Snorkeling" },
                { name: "2 Days 1 Night Tour Nusa Penida", path: "/tours/2-days-1-night-tour-nusa-penida", quote: "2 Days and 1 Night Nusa Penida Tour" },
                { name: "One Day Tour Nusa Lembongan", path: "/tours/one-day-tour-nusa-lembongan", quote: "One Day Nusa Lembongan Tour" }
            ]
        },
        {
            name: "Hotels", path: "/hotels", quote: "Hotels in Nusa Penida Partnership with Cherysa Tour"
        },
        {
            name: "Car Rental", path: "/car-rental-with-driver", quote: "Car Rental with Driver in Bali"
        },
        {
            name: "Book Now", path: "/book-now"
        },
        {
            name: "Sitemap", path: "/sitemap", mobileOnly: true
        }
    ]
}