module.exports = {
    destinationsList:[
        {
            name: "Angel Billabong",
            img: "images/contents/angel-billabong-mid-sq.jpg",
            desc: "a spectacular rock formation on Nusa Penida island's southwestern cliff edges. It's nearby Pasih Uug (Broken Beach), another popular site with unique limestone formations. The naturally formed rock lagoon offers a scenic seascape. You can descend into its crystal-clear rock pools for a swim or a soak",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/angel-billabong",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/angel-billabong.jpg"},
                        {img: "images/contents/angel-billabong-2.jpg"},
                        {img: "images/contents/angel-billabong-3.jpg"},
                        {img: "images/contents/angel-billabong-4.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.733426,
                        long: 115.4470798
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJiZ8Mtmpu0i0R_bG0qk8i-tk'
                    }
                }
            ]
        },
        {
            name: "Broken Beach",
            img: "images/contents/broken-beach-mid-sq.jpg",
            desc: "Broken Beach (Pasih Uug) is a scenic coastal formation on the southwestern edge of Nusa Penida island. The spot is marked by a hilly arch-like rock formation, which is the distinguishable landmark of the area. Set over the crashing waves of the open Indian Ocean, Broken Beach is a great spot for travel photographers and panoramic view seekers. The adjacent area is also home to grey long-tailed macaques. If you’re lucky, you may spot silhouettes of giant mantas near the surface of the deep blue water.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/broken-beach",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/broken-beach.jpg"},
                        {img: "images/contents/broken-beach-2.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7324634,
                        long: 115.449011
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJdX5lGBVu0i0RQ7h9o4ZPdlU'
                    }
                }
            ]
        },
        {
            name: "Kelingking Beach",
            img: "images/contents/klingking-beach-mid-sq.jpg",
            desc: "Kelingking Beach is a hidden beach in the village of Bunga Mekar, on the southwestern coast of Nusa Penida island. You can enjoy one of the most breath-taking views over the hills and small strip of white sand from atop a hill of the same name. The sight features a limestone headland covered in green, against the deep blue waters of the open Indian Ocean. This unique formation is reminiscent of a Tyrannosaurus Rex head, hence its nickname ‘T-Rex Bay’.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/kelingking-beach",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/klingking-beach.jpg", credit: "@journeyera"},
                        {img: "images/contents/klingking-beach-2.jpg", credit: "@mihtiander"},
                        {img: "images/contents/klingking-beach-3.jpg", credit: "@nicmorley"},
                        //{img: "images/contents/tree-of-love.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7527242,
                        long: 115.4697092
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJPeFviSBH0i0R0h1hnHjxmFw'
                    }
                }
            ]
        },
        {
            name: "Peguyangan Waterfall",
            img: "images/contents/peguyangan-waterfall-sq.jpg",
            desc: "Locally referred as Mata Air Peguyangan, is better described as Peguyangan Water Of Springs.<br>With it being more of a religious shrine than a waterfall, many people are disappointed by the end result. But the views on the walk down via the famous Nusa Penida blue stairs more than make up for it.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/peguyangan-waterfall",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/peguyangan-waterfall-1.jpg", credit: "@alexjgillett"},
                        {img: "images/contents/peguyangan-waterfall-2.jpg", credit: "@lolapantravels"},
                        {img: "images/contents/peguyangan-waterfall-3.jpg", credit: "@wanderersandwarriors"},
                        {img: "images/contents/peguyangan-waterfall-4.jpg", credit: "@wanderersandwarriors"},
                        {img: "images/contents/peguyangan-waterfall-5.jpg", credit: "@thenusapenida"},
                        {img: "images/contents/peguyangan-waterfall-6.jpg", credit: "@wanderersandwarriors"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7812546,
                        long: 115.5173131
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJVS_QJn960i0R3Pt_ZCHo0Eo'
                    }
                }
            ]
        },
        {
            name: "Teletubbies Hills",
            img: "images/contents/teletubbies-hills-mid-sq.jpg",
            desc: "Nusa Penida and Bali is identical to the overlay shores, beaches, white sands and sun, but there's something different when you're visiting the island of Nusa Penida. The southeast part of Bali island turn into a nice place to visit and you can find a similar green hills like in a movie called Teletubbies Hill",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/teletubbies-hills",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/teletubbies-hills.jpg"},
                        {img: "images/contents/teletubbies-hills-2.jpg", credit: "@galuh.pj"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7853848,
                        long: 115.5725955
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJYfEnJEV30i0RS9pcaNa604M'
                    }
                }
            ]
        },
        {
            name: "Raja Lima",
            img: "images/contents/raja-lima-mid-sq.jpg",
            desc: "The exquisite views of the beach area are undoubtedly. Beach, cliffs and Indonesian Ocean seem obvious. Small island is similar to Raja Ampat, West Papua. Many tourists call Atuh Beach areas with the nickname of Thousand Islands. This appeal become the choice of tourists visiting Nusa Penida. Cliffs and hills that surround the coast as shoulder to shoulder shows the beauty of the Atuh Beach overall.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/raja-lima",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/raja-lima.jpg"},
                        {img: "images/contents/tree-house.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7787791,
                        long: 115.614736
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJ419QjYF30i0R_Bn1jT5cVlo'
                    }
                }
            ]
        },
        {
            name: "Atuh Beach",
            img: "images/contents/atuh-beach-mid-sq.jpg",
            desc: "You will be amazed by the clarity of the turquoise waters, the gorgeous white sandy beaches, the towering high cliff walls, beautiful green nature, and best of all it should be all yours, your own private beach!",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/atuh-beach",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/atuh-beach.jpg"},
                        {img: "images/contents/atuh-beach-2.jpg"},
                        {img: "images/contents/atuh-beach-3.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7734116,
                        long: 115.61963
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJhYE8QJt30i0Rs8qDtRiE1iU'
                    }
                }
            ]
        },
        {
            name: "Crystal Bay",
            img: "images/contents/crystal-bay-beach-sq.jpg",
            desc: "Located on the west of Nusa Penida, is a suitable place to enjoy the beautiful views of the bay and rocky island. There you can sunbathe, swim and snorkel from the shore. Crystal Bay is very famous for Mola - mola (sunfish), so do not be surprised if you see a lot of dive boats anchored here.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/crystal-bay-beach",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/crystal-bay-beach-2.jpg"},
                        {img: "images/contents/crystal-bay-beach.jpg"},
                        {img: "images/contents/snorkeling-crystal-bay-beach.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7155815,
                        long: 115.4569574
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJrZrXTwhu0i0R1-hceK52v_o'
                    }
                }
            ]
        },
        {
            name: "Devil's Tears",
            img: "images/contents/devils-tears-mid-sq.jpg",
            desc: "Devil's Tears is a sparsely vegetated outcrop of land with dramatic limestone cliffs, on the west coast of Nusa Lembongan. Devil's Tears is situated right next to the stunning Dream Beach, where turtles play in the waves and lay their eggs on the beach. This is a magnificent spot to visit. Devil's Tears provides an area with many viewing spots for watching the massive swell crash against the cliffs below and spray high into the air.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/devils-tears",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/devils-tears.jpg"},
                        {img: "images/contents/devils-tears-2.jpg"},
                        {img: "images/contents/devils-tears-3.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.690743,
                        long: 115.4280638
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJNXQYfLRt0i0RALc3MIMT6QA'
                    }
                }
            ]
        },
        {
            name: "Giri Putri Cave",
            img: "images/contents/giri-putri-cave-mid-sq.jpg",
            desc: "Giri Putri cave is located near the village of Karangsari. After climbing a hundred works, you will reach the entrance of the cave so narrow that must kneel to enter. Inside, we are surprised by its volume. It is very dark inside, but at the end of the cave there is a large opening, and we do not know if the back wall was opened intentionally or if it was like that originally. Close to the exit, you will discover a place of worship for Hindu devotees, but also for the Buddhist and Confucian followers, evidenced by the presence of a statue of Dewi Kwan Im.",
            btnLabel: "Make a Tour",
            btnUrl: "/tours",
            detailUrl: "/destinations/giri-putri-cave",
            details:[
                {
                    name: "Gallery",
                    slides: [
                        {img: "images/contents/giri-putri-cave-2.jpg"},
                        {img: "images/contents/giri-putri-cave-3.jpg"},
                        {img: "images/contents/giri-putri-cave.jpg"}
                    ]
                },
                {
                    googleMap: {
                        lat: -8.7089515,
                        long: 115.583926
                    }
                },
                {
                    googleReview: {
                        placeID: 'ChIJf6i9TaF20i0R8_IaKI-kqMQ'
                    }
                }
            ]
        }
    ]
}