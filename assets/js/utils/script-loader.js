let callbackArr = []

export default {
    load(url, callback) {
        let scriptTag = document.getElementById("script-tag")
        if(callbackArr.indexOf(callback) == -1) callbackArr.push(callback)
        if(!scriptTag) {
            scriptTag = document.createElement("script")
            scriptTag.id = "script-tag"
            document.body.appendChild(scriptTag)
            scriptTag.onload = () => {
                scriptTag.setAttribute("loaded", "true")
                callbackArr.map(f => {
                    f.call()
                    return f
                })
                callbackArr = []
            }
            scriptTag.src = url
        } else if(scriptTag.hasAttribute("loaded")) {
            callbackArr.map(f => {
                f.call()
                return f
            })
            callbackArr = []
        }
    },
    dispose() {
        let scriptTag = document.getElementById("script-tag")
        if(scriptTag) scriptTag.parentNode.removeChild(scriptTag)
        callbackArr = []
    }
}