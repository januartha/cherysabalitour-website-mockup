let initiated = false;
let onScrollCallbackArr = [];
const registerListener  = (e, f) => {
    if(window.addEventListener) {
        window.addEventListener(e, f)
    } else {
        window.attachEvent("on" + e, f)
    }
}
const unRegisterListener  = (e, f) => {
    if(window.removeEventListener) {
        window.removeEventListener(e, f)
    } else {
        window.detachEvent("on" + e, f)
    }
}
const onScroll = () => {
    onScrollCallbackArr.map((f) => {
        f.call()
        return f
    })
}
const registerOnScrollCallback = (callback) => {
    let index = onScrollCallbackArr.indexOf(callback);
    if(index == -1) onScrollCallbackArr.push(callback);
    //
    init();
}
const unRegisterOnScrollCallback = (callback) => {
    let index = onScrollCallbackArr.indexOf(callback);
    if(index > -1) onScrollCallbackArr.splice(index, 1);
}
const dispose = () => {
    unRegisterListener("scroll", onScroll);
    unRegisterListener("resize", onScroll);
    //reset
    initiated = false
    onScrollCallbackArr = [];
}
const init = () => {
    if(!initiated)
    {
        registerListener("scroll", onScroll)
        registerListener("resize", onScroll)
    }
    initiated = true
}
export default {
    registerOnScrollCallback,
    unRegisterOnScrollCallback,
    dispose
}