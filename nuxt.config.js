//const webpack = require("webpack")
const tours = require("./assets/data/tours")
const destinations = require("./assets/data/destinations")

module.exports = {
    head: {
        htmlAttrs: {
            lang: 'en'
        },
        title: "Nusa Penida Tour with Cherysa Bali Tour",
        titleTemplate: "%s | Cherysa Bali Tour",
        meta: [
            { charset: 'utf-8' },
            { name:"google-site-verification", content:"ikaNaw186Qjr7RE7CXcAZ2b1VSrYN8oakmVc9lYVvK0" },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            //manifest theme
            { name: 'apple-mobile-web-app-title', content: 'Cherysa Bali Tour' },
            { name: 'application-name', content: 'Cherysa Bali Tour' },
            { name: 'msapplication-TileColor', content: '#fe6601' },
            { name: 'msapplication-TileImage', content: '/mstile-144x144.png' },
            { name: 'theme-color', content: '#fe6601' },
            //author
            //{ name: 'author', content: 'Cherysa Bali Tour' },
            //robots
            { name: "robots", content: "noodp,noydir" },
            //social media
            { property: 'og:site_name', content: 'Cherysa Bali Tour' },
            { property: 'og:type', content: 'website' },
            { property: 'og:locale', content: 'en_US' },
            { name: 'twitter:card', content: 'summary' },
            { name: 'twitter:site', content: '@cherysatour' },
            { name: 'twitter:creator', content: '@cherysatour' }
        ],
        link: [
            { rel: 'apple-touch-icon', type: 'image/x-icon', size: "180x180", href: '/apple-touch-icon.png' },
            { rel: 'icon', type: 'image/x-icon', size: "32x32", href: '/favicon-32x32.png' },
            { rel: 'icon', type: 'image/x-icon', size: "16x16", href: '/favicon-16x16.png' },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: "preload", href:"https://fonts.googleapis.com/css?family=Catamaran", as:"style"},
            { rel: "stylesheet", href:"https://fonts.googleapis.com/css?family=Catamaran", bottom: true},
            { rel: "preconnect", href:"https://fonts.gstatic.com/", crossorigin:"crossorigin"},
            //{ rel: "preload", href: "/css/style.css", as: "style" },
            //{ rel: "stylesheet", href: "/css/style.css", bottom: true },
            //{ rel: "preload", href: "./css/style.css", as:"style" },
            //{ rel: "preload", href: "https://code.jquery.com/jquery-1.11.0.min.js", as:"script"},
            //{ rel: "preload", href: "https://code.jquery.com/jquery-migrate-1.2.1.min.js", as:"script"},
            //{ rel: "preload", href: "https://code.jquery.com/jquery-1.2.1.min.js" },
            //{ rel: "preload", href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js", as:"script"}

        ],
        script: [
            //{ src: "https://code.jquery.com/jquery-1.11.0.min.js", body: true },
            //{ src: "https://code.jquery.com/jquery-migrate-1.2.1.min.js", body: true },
            //{ src: "https://code.jquery.com/jquery-1.2.1.min.js", body: true },
            //{ src: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js", body: true }//,
            //{ innerHTML: '{ "@context": "http://schema.org", "@type": "TravelAgency", "url": "http://www.cherysabalitour.com", "image": "https://cherysabalitour.com/images/contents/klingking-beach.jpg", "name": "Full Day Nusa Penida Tour with Cherysa Tour", "description" : "We manage a full day tour to fit your time and budget but still enough to enjoy and get experience on exploring magical Nusa Penida tourist destinations.", "potentialAction": { "@type": "Action", "image":"https://cherysabalitour.com/images/contents/snorkeling-crystal-bay-beach.jpg", "name":"Snorkeling" }, "contactPoint": { "@type": "ContactPoint", "telephone": "+62 081 139 93366", "contactType": "Sales and Booking" }, "address": { "@type": "PostalAddress", "streetAddress": "Jalan Waru Klumpu, Banjar Waru", "addressLocality": "Desa Klumpu, Nusa Penida, Klungkung", "addressRegion": "Bali", "postalCode": "80771", "addressCountry": "Indonesia" }, "telephone": "+62  081 139 93366", "priceRange": "start from $40.00"}', type:"application/ld+json" }
        ],
        //disable sanitize to quote https://github.com/nuxt/nuxt.js/issues/2230#issuecomment-346997270
        __dangerouslyDisableSanitizers: ['script']    
    },
    loading: {
        color: '#FE6601',
        height: '4px'
    },
    css: [
        //"@/assets/css/main.scss",
        "@/assets/css/style.scss"
    ],
    generate: {
        //disable to make subfolder for each route, instead generate html file per route
        subFolders: false,
        //generate dynamic routes
        routes: [
            '/destinations/index',
            '/tours/index'
        ]
        .concat(tours.packages.map(item => item.detailUrl))
        .concat(destinations.destinationsList.map(item => item.detailUrl))
    },
    build: {
        //extract css
        extractCSS: true,
        //generate js filenames without chunkhash
        filenames: {
            css: '[name].css',
            manifest: 'manifest.js',
            vendor: 'common.js',
            app: 'app.js',
            chunk: '[name].js'
        },
        plugins: [
            //extra webpack plugins add global jquery
            /*
            new webpack.ProvidePlugin({
                $: "jquery",
                jquery: "jquery"
            })*/
        ]
    }

}