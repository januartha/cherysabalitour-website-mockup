import data from "@/assets/data/menu.js"
import splash from "@/components/splash"
import headerMenu from "@/components/header"
import footerPage from "@/components/footer"
import instaFeed from "@/components/insta-feed"
export default {
    components: {
        splash, headerMenu, instaFeed, footerPage
    },
    head: {
        link: [
            { rel: "preload", href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css", as: "style" },
            { rel: "stylesheet", href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css", bottom: true }
        ] 
    },
    data() {
        return {
            list: data.menu
        }
    },
    watch: {
        "$route"(to, from) {
            //
            //if(document.querySelector(".quote")) document.querySelector(".quote").classList.add("hidden");
            //
            //console.log(to.name, from.name);
            window.scrollTo(0, 0);
        }
    }
}